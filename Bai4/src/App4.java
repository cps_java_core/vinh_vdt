
/**
 * @ Author: Vo Duy Thien Vinh
 * @ Language: Java
 * @ Email: thienvinhvpv@gmail.com
 * @ Website: https://codepsoft.com/
 * @ Lab 4
 *
 */

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.Arrays;


public class App4 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(final String[] args) throws Exception {
        
        hashMap();
    }

    // #region Function read Integer
    public static int readInteger() {
        int value = 0;
        while (true) {
            System.out.println("Enter a positive integer x: ");
            try {
                final String temp = scanner.nextLine();
                value = Integer.parseInt(temp);
                if (value > 0) {
                    break;
                } else {
                    System.out.println("ERROR !!! You enter negative numbers");
                    continue;
                }
            } catch (final Exception e) {
                System.out.println("ERROR !!! \nTHE REQUEST: IS A POSITIVE INTEGER ");
            }
        }
        return value;
    }
    // #endregion

    // #region Function read Integer > 1
    public static int readInteger_1() {
        int value = 0;
        while (true) {
            System.out.println("Enter a positive integer x (x > 1): ");
            try {
                final String temp = scanner.nextLine();
                value = Integer.parseInt(temp);
                if (value > 1) {
                    break;
                } else {
                    System.out.println("ERROR !!! You enter negative numbers (x > 1)");
                    continue;
                }
            } catch (final Exception e) {
                System.out.println("ERROR !!! \nTHE REQUEST: IS A POSITIVE INTEGER (X > 1) ");
            }
        }
        return value;
    }

    // #region Function 1 + (1/(2x3))+ (1/(3x3))+...+(1/(nx3)) (làm tròn 3 chữ số)
    // Function use For
    public static float v_functionCalculate() {
        final int n = readInteger_1();
        int sum = 1;
        for (int i = 2; i <= n; i++) {
            sum += (1 / (i * 3));
        }
        return Math.round(sum);
    }

    // Function use While
    public static float v_functionCalculate1() {
        final int n = readInteger_1();
        int sum = 1;
        int i = 2;
        while (i <= n) {
            sum += (1 / (i * 3));
            i++;
        }
        return Math.round(sum);
    }

    // Function use Do While
    public static float v_functionCalculate2() {
        final int n = readInteger_1();
        int sum = 1;
        int i = 2;
        do {
            sum += (1 / (i * 3));
            i++;
        } while (i <= n);
        return Math.round(sum);
    }
    // #endregion

    // #region Question 2 Calculate
    public static float v1_functionCalculate() {
        final int n = readInteger();
        float sum = 1;
        float k = 1;
        for (float i = 0; i <= n; i++) {
            k *= ((2 * (i + 1)) / ((2 * i) + 3));
            sum += k;
        }
        System.out.println("Result: " + sum);
        return sum;
    }

    public static float v1_functionCalculate_1() {
        final int n = readInteger();
        float x = 2;
        float y = 3;
        float sum = 1;
        float T = 1;
        for (int i = 0; i <= n; i++) {
            final float K = (x / y);
            T *= K;
            sum += T;
            x += 2;
            y += 2;
        }
        System.out.println(sum);
        return sum;
    }
    // #endregion

    //#region Question 4 (Triangle)
    //Function Main Triangle
    //Issue: Phan loai tam giac: neu ko phai tam giac lap tuc dung chuong trinh 
    public static void mainTriangle(){
        float a = 0f,b = 0f,c = 0f;
        a = readFloatTriangle("a");
        b = readFloatTriangle("b");
        c = readFloatTriangle("c");
        float p = perimeterTriangle_1(a, b, c);
        System.out.println("Perimeter Triangle: " + p);
        distributeTriangle(a, b, c);
        float s = areaTriangle(a, b, c);
        System.out.println("Area Triangle: " + s);

    }

    // Function 1/2 Perimeter Triangle
    public static float perimeterTriangle(float a, float b, float c){
         return ((float)(a+b+c)/2);
    }

    // Function Perimeter Triangle
    public static float perimeterTriangle_1(float a, float b, float c){
        return a+b+c;
    }

    // Function Distribute Triangle
    public static void distributeTriangle(float a, float b, float c) {
        if (a < b + c && b < a + c && c < a + b) {
            if (a * a == b * b + c * c || b * b == a * a + c * c || c * c == a * a + b * b) {
                System.out.println("This is a right triangle"); // tam giac vuong
            } else if (a == b && b == c && c == a) {
                System.out.println("This is an equilateral triangle"); // tam giac deu
            } else if (a == b || a == c || b == c) {
                System.out.println("This is an isosceles triangle"); // tam giac can
            } else if (a * a > b * b + c * c || b * b > a * a + c * c || c * c > a * a + b * b) {
                System.out.println("This is an obtuse triangle"); // tam giac tu
            } else {
                System.out.println("This is an acute triangle"); // tam giac nhon
            }
        } else {
            System.out.println("This is not a triangle");
        }
    }

    //Function Area Triangle
    public static float areaTriangle(float a, float b, float c){
        float p = perimeterTriangle(a,b,c);
        return (float)Math.sqrt(p * (p-a) * (p-b) * (p-c));
    
    }

    //Function Read Float Triangle
    public static float readFloatTriangle(String x) {
        float value = 0;
        while (true) {
            System.out.println("Enter the side length " + x + ": ");
            try {
                final String temp = scanner.nextLine();
                value = Float.parseFloat(temp);
                if (value > 0) {
                    break;
                } else {
                    System.out.println("ERROR !!! You enter negative numbers");
                    continue;
                }
            } catch (final Exception e) {
                System.out.println("ERROR !!! \nTHE REQUEST: IS A POSITIVE INTEGER ");
            }
        }
        return value;
    }
    //#endregion
   
    //#region Change Money HashMap
    public static void doitien(){
        HashMap<String, Integer> money = new HashMap<String, Integer>();
        money.put("USD", 23000); 
        money.put("EUR", 26000);
        money.put("YPJ", 210);
        money.put("RUB", 308);
        
        System.out.println(money.containsValue(23000)); //value
        System.out.println(money.containsKey("USD")); //key
        System.out.println(money.entrySet());
        System.out.println(money.keySet());
        System.out.println(money.values());

        
    }
    //#endregion


    //#region !!!3. Thuc hanh ArrayList, List
    //Function III_1
    /** Cho nhập vào một mảng các phần tử kiểu String và một kí tự bất kì, tìm tất cả
    * các chữ cái bắt đầu từ kí tự bất kì đó
    */
    public static void arrayList(){
        
        ArrayList<String> arrayList = new ArrayList<String>();
        System.out.println("Invite you to enter the string: \n(Please enter 'q' to exit)");
        
        while(true){
                String answer = scanner.nextLine();
                if (answer.equals("q")){
                    break;
                } 
                if (answer.isEmpty()){
                    continue;
                }
                arrayList.add(answer);
        }
        System.out.println("Value: " + arrayList);
        System.out.println("Size of ArrayList: " + arrayList.size());

        System.out.println("Input char check: ");
        char check = scanner.next().charAt(0);

        for(int i=0; i < arrayList.size(); i++){
            if((arrayList.get(i)).charAt(0) == check){
                System.out.println("Strings begin with a character " + "\'" + check + "\'" +  ": "+ arrayList.get(i));
            }
        }
    }

    //Function III_2
    /**Chuyển đổi chuỗi sau thành một mảng: “Hello_I_am_Iron_Man!”. 
     */
    public static void stringChangeArray(){
        String str = "Hello_I_AM_I_RON_MAN!";
        char[] ch = str.toCharArray();
        for(int i = 0; i < ch.length; i++){
            System.out.println(ch[i]);
        }
    } 

    /** 
     * Chuyển đổi chuỗi sau thành một mảng: “Hello_I_am_Iron_Man!”.
     * Array List
     */
    public static void stringChangeArrayList(){
        String iron = "Hello_I_am_Iron_Man!";
        List<String> array = new ArrayList<String>(); //Why List ???
        String str[] = iron.split("_");
        array = Arrays.asList(str);
        for(String i : array){
            System.out.println(i);
        }
    }


    /**
     * Chuyển một mảng thành chuỗi được nối với nhau bằng dấu gạch dưới (_)
     */
    public static void arrayListChangeString(){
        List<String> array = new ArrayList<String>();
        System.out.println("Invite you to enter the string: \n(Please enter 'q' to exit)");
        while(true){
            String answer = scanner.nextLine();
                if (answer.equals("q")){
                    break;
                } 
                if (answer.isEmpty()){
                    continue;
                }
                array.add(answer);
        }

        String listString = new String();
        for(String s : array) {
            listString += s + "_";
        }
        listString = listString.substring(0, listString.length() - 1);
        System.out.println(listString);
    }


    /**
     * Tạo một kiểu dữ liệu HashMap, cho phép người dùng nhập vào Key là kiểu
     *  String và value là kiểu String. Xuất ra cấu trúc json đó.
     */
    public static void hashMap(){
        Map<String, String> testHashMap = new HashMap<String, String>();
        System.out.println("Invite you to enter the key and value: \n(Please enter 'q' to exit)");

        while(true){
            System.out.print("Key: ");
            String key = scanner.nextLine();
            if(key.equals("q")){
                System.out.println("You exit !!!");
                break;
            }

            System.out.print("Value: ");
            String value = scanner.nextLine();
            if(value.equals("q")){
                System.out.println("You exit !!!");
                break;
            }
            
            if(key.isEmpty() || value.isEmpty()){
                continue;
            }
            testHashMap.put(key, value);
        }

        System.out.println("Size: " + testHashMap.size());
        System.out.println("Entry: " + testHashMap);

        System.out.println("{");
        for(Entry<String, String> entry : testHashMap.entrySet()){
            System.out.println("\""+ entry.getKey() + "\"" + " : " + "\"" + entry.getValue() + "\"" + ",");
        }
        System.out.println("}");
    }

    /**
     * Tạo một kiểu dữ liệu HashMap, cho phép người dùng nhập vào Key là kiểu
     * String và value là kiểu bất kì. Xuất ra cấu trúc Json đó. 
     */ 

    public static void hashMap_1(){
       
    }
    
    // #endregion
}
