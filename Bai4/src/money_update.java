import java.util.Scanner;
import java.util.regex.*;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.Map;

public class money_update {
    public static Map<String, Integer> money = new HashMap<String, Integer>();
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) throws Exception {
        money.put("USD", 23000); // key, value
        money.put("SGD", 17000);
        money.put("EUR", 26000);
        money.put("RUB", 308);
        money.put("GPB", 30000);

        String x1 = "Enter the amount you want to exchange: ";
        String x = "Enter default money: ";

        System.out.println(money);
        String denominationMoney = new String();
        denominationMoney = readMoney_Update(x);

        String denominationMoneyChange = new String();
        denominationMoneyChange = readMoney_Update(x1);

        float inputMoney = inputMoney();

        switch (denominationMoney) {
            case "USD":
                System.out.print("Result: ");
                System.out.println(moneyUSD(inputMoney, denominationMoneyChange));
                break;

            case "SGD":
                System.out.print("Result: ");
                System.out.println(moneySGD(inputMoney, denominationMoneyChange));
                break;

            case "EUR":
                System.out.print("Result: ");
                System.out.println(moneyEUR(inputMoney, denominationMoneyChange));
                break;

            case "RUB":
                System.out.print("Result: ");
                System.out.println(moneyRUB(inputMoney, denominationMoneyChange));
                break;

            case "GPB":
                System.out.print("Result: ");
                System.out.println(moneyGPB(inputMoney, denominationMoneyChange));
                break;

            default:
                System.out.println("Chim cut + Xoi xeo");
                break;
        }

        System.out.println("\n\n");
    }

    //#region Money
    public static float moneyUSD(float x, String x1) {
        for (Entry<String, Integer> entry : money.entrySet()) {
            if (x1.equals(entry.getKey())) {
                if (x1.equals("USD")) {
                    break;
                }

                if (x1.equals("SGD")) {
                    x *= money.get("USD");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("EUR")) {
                    x *= money.get("USD");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("RUB")) {
                    x *= money.get("USD");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("GPB")) {
                    x *= money.get("USD");
                    x = x / entry.getValue();
                    break;
                }
            } else {
                continue;
            }
        }
        return x;
    }

    public static float moneySGD(float x, String x1) {
        for (Entry<String, Integer> entry : money.entrySet()) {
            if (x1.equals(entry.getKey())) {
                if (x1.equals("SGD")) {
                    break;
                }

                if (x1.equals("USD")) {
                    x *= money.get("SGD");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("EUR")) {
                    x *= money.get("SGD");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("RUB")) {
                    x *= money.get("SGD");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("GPB")) {
                    x *= money.get("SGD");
                    x = x / entry.getValue();
                    break;
                }
            } else {
                continue;
            }
        }
        return x;
    }

    public static float moneyEUR(float x, String x1) {
        for (Entry<String, Integer> entry : money.entrySet()) {
            if (x1.equals(entry.getKey())) {
                if (x1.equals("EUR")) {
                    break;
                }

                if (x1.equals("SGD")) {
                    x *= money.get("EUR");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("USD")) {
                    x *= money.get("EUR");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("RUB")) {
                    x *= money.get("EUR");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("GPB")) {
                    x *= money.get("EUR");
                    x = x / entry.getValue();
                    break;
                }
            } else {
                continue;
            }
        }
        return x;
    }

    public static float moneyRUB(float x, String x1) {
        for (Entry<String, Integer> entry : money.entrySet()) {
            if (x1.equals(entry.getKey())) {
                if (x1.equals("RUB")) {
                    break;
                }

                if (x1.equals("SGD")) {
                    x *= money.get("RUB");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("EUR")) {
                    x *= money.get("RUB");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("USD")) {
                    x *= money.get("RUB");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("GPB")) {
                    x *= money.get("RUB");
                    x = x / entry.getValue();
                    break;
                }
            } else {
                continue;
            }
        }
        return x;
    }

    public static float moneyGPB(float x, String x1) {
        for (Entry<String, Integer> entry : money.entrySet()) {
            if (x1.equals(entry.getKey())) {
                if (x1.equals("GPB")) {
                    break;
                }

                if (x1.equals("SGD")) {
                    x *= money.get("GPB");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("EUR")) {
                    x *= money.get("GPB");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("RUB")) {
                    x *= money.get("GPB");
                    x = x / entry.getValue();
                    break;
                }

                if (x1.equals("USD")) {
                    x *= money.get("GPB");
                    x = x / entry.getValue();
                    break;
                }
            } else {
                continue;
            }
        }
        return x;
    }
    //#endregion

    // Function readMoney_Update
    public static String readMoney_Update(String x) {
        String readMoney = new String();
        String digit = "\\D+";
        String special = "[a-zA-Z0-9]*"; // * la so co khop vs cac ki tu phia truoc ko ?
        while (true) {
            System.out.println(x);
            try {
                readMoney = sc.nextLine();
                readMoney = readMoney.toUpperCase();

                if (readMoney.length() == 3) { // khong phai la so thi vao if (true), la so thi vao else continue
                                               // (false)
                    if (((Pattern.matches(digit, readMoney)) && (Pattern.matches(special, readMoney)))) {
                        if ((readMoney.equals("USD")) || (readMoney.equals("SGD")) || (readMoney.equals("EUR"))
                                || (readMoney.equals("RUB")) || (readMoney.equals("GPB"))) {
                            break;
                        } else {
                            continue;
                        }
                    } else {
                        System.out.println("You have entered special characters or numbers. Request to re-enter");
                        continue;
                    }

                } else {
                    continue;
                }
            } catch (Exception e) {
                System.out.println("Request Again");
                continue;
            }
        }
        return readMoney;
    }

    // String ReadMoney
    public static String readMoney(String x) {
        String readMoney = new String();
        while (true) {
            System.out.println(x);
            try {
                readMoney = sc.nextLine();
                readMoney = readMoney.toUpperCase();
                boolean check = true;
                if (readMoney.length() == 3) {
                    for (int i = 0; i < readMoney.length() - 1; i++) {
                        if (readMoney.charAt(i) >= '0' && readMoney.charAt(i) <= '9') {
                            check = false;
                            break;
                        } else {
                            check = true;
                            continue;
                        }
                    }

                    if (check == true) {
                        break;
                    } else {
                        continue;
                    }

                } else {
                    continue;
                }
            } catch (Exception e) {
                System.out.println("Request Again");
                continue;
            }
        }
        return readMoney;
    }

    // Function Input Money
    public static float inputMoney() {
        float inputMoney = 0;
        while (true) {
            System.out.print("Input Money: ");
            try {
                String temp = sc.nextLine();
                inputMoney = Float.parseFloat(temp);
                if (inputMoney > 0) {
                    break;
                } else {
                    continue;
                }
            } catch (Exception e) {
                System.out.println("ERROR: Request Again Input Money");
                continue;
            }
        }
        return inputMoney;
    }

}
