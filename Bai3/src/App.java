/**
 * @ Author: Vo Duy Thien Vinh
 * @ Language: Java
 * @ Email: thienvinhvpv@gmail.com
 * @ Website: https://codepsoft.com/
 * @ Lab 3
 * 
 */
import java.util.Scanner;
import java.util.Random;

public class App {
    public static Scanner scanner = new Scanner(System.in);
    public static Random random = new Random();

    public static void main(String[] args) throws Exception {
        
    }

    // #region 1 Complete readInteger
    public static int readInteger() {
        int value = 0;
        while (true) {
            System.out.println("Enter a positive integer x: ");
            try {
                String temp = scanner.nextLine();
                value = Integer.parseInt(temp);
                if (value > 0) {
                    break;
                } else {
                    System.out.println("ERROR !!! You enter negative numbers");
                    continue;
                }
            } catch (Exception e) {
                System.out.println("ERROR !!! \nTHE REQUEST: IS A POSITIVE INTEGER ");
            }
        }
        return value;
    }
    // #endregion

    // #region 1 Complete readValueArray
    public static void readValueArray() {
        System.out.println("Enter the elements of the array: ");
        int n = readInteger();
        int array[] = new int[n];
        for (int i = 0; i < n; i++) {
            while (true) {
                System.out.printf("a[%d] = ", i);
                try {
                    String temp = scanner.nextLine();
                    array[i] = Integer.parseInt(temp);

                    if (array[i] > 0) {
                        break;
                    } else {
                        System.out.println("You enter negative numbers ");
                        continue;
                    }
                } catch (Exception e) {
                    System.out.println("ERROR !!! \nTHE REQUEST: IS A POSITIVE INTEGER");
                }
            }
        }
        findMinValueReturnArray(array);
        outputArray(array); // phai reset cai mang, khong the dung mang cu.
    }
    // #endregion

    // #region 2 Output Array
    public static void outputArray(int[] array) {
        System.out.println("The value of the array is: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
    // #endregion

    // #region 3 Find Maximum Value Array
    // Function: Finds the largest value in the array and no return the value
    public static void findMaxValue(int arr[]) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        System.out.println("Result: " + max);
    }

    // Function: Finds the largest value in the array and returns the value in the
    // array
    public static int[] findMaxValueReturnArray(int arr[]) {
        int max = Integer.MIN_VALUE;
        for (int i : arr) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return arr;
    }

    // Function : Finds the largest value in the array and returns the value
    public static int findMaxValueReturn(int arr[]) {
        int max = Integer.MIN_VALUE;
        for (int i : arr) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }
    // #endregion

    // #region 3 Find Min Value Array
    // Function: Finds the smallest value in the array and no returns the value
    public static void findMinValue(int arr[]) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i <= arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        System.out.println("Result: " + min);
    }

    // Function: Finds the smallest value in the array and returns the value in the
    // array
    public static int[] findMinValueReturnArray(int arr[]) {
        int min = Integer.MAX_VALUE;
        for (int i : arr) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        return arr;
    }

    // Function: Finds the smallest value in the array and returns the value
    public static int findMinValueReturn(int arr[]) {
        int min = Integer.MAX_VALUE;
        for (int i : arr) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        return min;
    }
    // #endregion

    // #region 4 Find Maximum Values Array

    public static int[] findMinValueReturnArrayCau4(int arr[]) {
        int min = Integer.MIN_VALUE;
        for (int i : arr) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        return arr;
    }

    
    // #endregion

    // #region 5 Odd and Even numbers
    // Function Main 5
    // Function Read Value Array Random
    public static void readValueArrayRandom() {
        int n = readInteger();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = random.nextInt(n * 10);
        }
    }

    // Function Output Value Array Random
    public static void outputValueArrayRandom(int arr[]) {
        System.out.println("The value array random is: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(" " + arr[i]);
        }
    }

    // 5a
    // Function The total is even
    public static void theTotalIsEven(int arr[]) {
        int sum = 0;
        System.out.print("\n");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                sum += arr[i];
            }
        }
        System.out.println("The total is even is: " + sum);
    }

    // Function The total is even return
    public static int theTotalIsEvenReturn(int arr[]) {
        int sum = 0;
        System.out.println("\n");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                sum += arr[i];
            }
        }
        return sum;
    }

    // 5b
    // Function The total is odd
    public static void theTotalIsOdd(int arr[]) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                sum += arr[i];
            }
        }
        System.out.println("The total is odd is: " + sum);
    }

    // Function The total is odd return
    public static int theTotalIsOddReturn(int arr[]) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                sum += arr[i];
            }
        }
        return sum;
    }

    // 5c
    // Function Find Odd Numbers
    public static void findOddNumbers(int arr[]) {
        int oddNumber = 0;
        System.out.println("The odd numbers in the array are:  ");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                oddNumber = arr[i];
                System.out.println(" " + oddNumber);
            }
        }
    }

    // Function Find Odd Numbers Return Value
    public static int[] findOddNumbersReturn(int arr[]) {
        int result[] = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                result[i] = arr[i];
            }
        }
        return result;
    }

    // 5d
    // Function Find Even Numbers
    public static void findEvenNumbers(int arr[]) {
        int evenNumber = 0;
        System.out.println("The even numbers in the array are:  ");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                evenNumber = arr[i];
                System.out.println(" " + evenNumber);
            }
        }
    }

    // Function Find Even Numbers Return Value TEST
    public static int[] findEvenNumbersReturn(int arr[]) {
        int result[] = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                result[i] = arr[i];
            }
        }
        return result;
    }
    // #endregion

    // #region 6 The divisible numbers by 2 and 5
    // Function Read Value Array Random
    public static void readValueArrayRandom6() {
        int n = readInteger();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = random.nextInt(n * 1000);
        }
    }

    // Function Divisible By 2 And 5
    public static void divisibleBy2And5(int arr[]) {
        int divisible = 0;
        System.out.println("The divisible numbers by 2 and 5 in the array are: ");
        for (int i = 0; i < arr.length; i++) {
            if ((arr[i] % 2 == 0) && (arr[i] % 5 == 0)) {
                divisible = arr[i];
                System.out.println(" " + divisible);
            }
        }
    }

    // Function Divisible By 2 And 5 Return TEST
    public static int[] divisibleBy2and5Return(int arr[]) {
        int result[] = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if ((arr[i] % 2 == 0) && (arr[i] % 5 == 0)) {
                result[i] = arr[i];
            }
        }
        return result;
    }
    // #endregion

    // #region 7 Recursive
    // Function Factorial No Return
    public void factorial(int n) {
        int recursive = 1;
        for (int i = 1; i <= n; i++) {
            recursive *= i;
        }
        System.out.println("Recursive is: " + recursive);
    }

    // Function Factorial Return Value
    public int factorialReturn(int n) {
        int recursive = 1;
        for (int i = 1; i <= n; i++) {
            recursive *= i;
        }
        return recursive;
    }

    // Function Factorial Recursive
    public int factorialRecursive(int n) {
        if (n == 1) {
            return 1;
        }
        return n * factorialRecursive(n - 1);
    }
    // #endregion

    // #region 8 Find the last element Even Number
    // Function Find the last Even No Return (i--)
    public static void findTheLastEven(int arr[]) {
        int theLastEven = 0;
        for (int i = arr.length; i > 0; i--) {
            if (arr[i] % 2 == 0) {
                theLastEven = arr[i];
                break;
            }
        }
        System.out.println("The last element even numbers is: " + theLastEven);
    }

    // Function Find the last Even Return Value(i--)
    public static int findTheLastEvenReturn(int arr[]) {
        int theLastEven = 0;
        for (int i = arr.length; i > 0; i--) {
            if (arr[i] % 2 == 0) {
                theLastEven = arr[i];
                break;
            }
        }
        return theLastEven;
    }

    // Function Find the last Even No Return (i++)
    public static void findTheLastEven_1(int arr[]) {
        int theLastEven = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                theLastEven = arr[i];
            }
        }
        System.out.println("The last element even numbers is: " + theLastEven);
    }

    // Function Find the last Even Return Value(i++)
    public static int findTheLastEvenReturn_1(int arr[]) {
        int theLastEven = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                theLastEven = arr[i];
            }
        }
        return theLastEven;
    }

    // #endregion

    // #region 9 Find the last element Odd Number
    //Function Find the last Even No Return Value(i--)
    public static void findTheLastOdd(int arr[]) {
        int theLastOdd = 0;
        for (int i = arr.length; i > 0; i--) {
            if (arr[i] % 2 != 0) {
                theLastOdd = arr[i];
                break;
            }
        }
        System.out.println("The last element odd number is: " + theLastOdd);
    }

    //Function Find the last Return Value (i--)
    public static int findTheLastOddReturn(int arr[]) {
        int theLastOdd = 0;
        for (int i = arr.length; i > 0; i--) {
            if (arr[i] % 2 != 0) {
                theLastOdd = arr[i];
                break;
            }
        }
        return theLastOdd;
    }

    //Function Find the last No Return Value (i++)
    public static void findTheLastOdd_1(int arr[]) {
        int theLastOdd = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                theLastOdd = arr[i];
            }
        }
        System.out.println("The last element odd number is: " + theLastOdd);
    }
    
    //Function Find the last Return Value(i++) 
    public static int findTheLastOddReturn_1(int arr[]) {
        int theLastOdd = 0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] %2 != 0){
                theLastOdd = arr[i];
            }
        }
        return theLastOdd;
    }

    // #endregion

    //#region 10 String
    public static void readString(){
        int n = readInteger();
        String arr[] = new String[n];
        for(int i=0;i < arr.length; i++ ){
            System.out.printf("a[%d] = ", i);
            arr[i] = scanner.nextLine();   
        }

        System.out.println("The value of array string: ");
        for(int i = 0; i < arr.length; i++){
            System.out.printf("a[%d] = ", i);
            System.out.println(arr[i] + " " + arr[i].length());
        }

    }
    //#endregion
    // #region Input positive integer
    public static int inputPositiveInteger() {
        int n = 0;
        try {
            System.out.println("Enter a positive integer x: ");
            n = scanner.nextInt();

            if (n <= 0) { // n <= 0
                do {
                    System.out.println("Required to re-enter positive integer x: ");
                    n = scanner.nextInt();
                } while (n <= 0);
            }
        } catch (Exception e) {
            // Char, ki tu dac biet, float, ...
            System.out.println("The request is a positive integer");
            System.exit(0);
        }
        return n;
    }
    // #endregion

    // #region Input Array
    public static void inputArray() {
        int n = 0;
        try {
            System.out.println("Enter the number of elements n of the array: \nTHE REQUEST: IS A POSITIVE INTEGER");
            n = scanner.nextInt();
            if (n <= 0) { // n <= 0
                do {
                    System.out.println("Required to re-enter positive integer x: ");
                    n = scanner.nextInt();
                } while (n <= 0);
            }
        } catch (Exception e) {
            System.out.println("ERROR !!! \nTHE REQUEST: IS A POSITIVE INTEGER");
            System.exit(0);
        }

        int array[] = new int[n];
        try {
            System.out.println("Enter the elements of the array: \n");
            for (int i = 0; i < n; i++) {
                System.out.printf("a[%d] = ", i);
                array[i] = scanner.nextInt();
            }
        } catch (Exception e1) {
            System.out.println("ERROR !!! \nTHE REQUEST: IS A POSITIVE INTEGER");
            System.exit(0);
        }
        outputArray(array);
    }
    // #endregion

    // #region Test
    public static int inputPositiveInteger(int n) {
        // int n = 0;
        try {
            System.out.println("Enter the number of elements n of the array: \nTHE REQUEST: IS A POSITIVE INTEGER");
            n = scanner.nextInt();
            if (n <= 0) { // n <= 0
                do {
                    System.out.println("Required to re-enter positive integer x: ");
                    n = scanner.nextInt();
                } while (n <= 0);
            }
        } catch (Exception e) {
            System.out.println("ERROR !!! \nTHE REQUEST: IS A POSITIVE INTEGER");
            System.exit(0);
        }
        return n;
    }

    public static void inputValueArray(int n, int array[]) {
        try {
            System.out.println("Enter the elements of the array: \n");
            for (int i = 0; i < n; i++) {
                System.out.printf("a[%d] = ", i);
                array[i] = scanner.nextInt();
            }
        } catch (Exception e1) {
            System.out.println("ERROR !!! \nTHE REQUEST: IS A POSITIVE INTEGER");
            System.exit(0);
        }
    }

    // Function Output Value Array
    public static void outputValueArray(int array[]) {
        System.out.println("The value array is : ");
        for (int i = 0; i < array.length; i++) {
            System.out.printf("a[%d] = ", i);
            array[i] = scanner.nextInt();
        }
    }
    // #endregion

    
}
